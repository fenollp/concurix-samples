# Heartbeat

# heart_:beat/3
`heart_:beat(Period, Beats, Size)` creates a heart beating every `Period`
seconds, for `Beats` times. It in fact initiate a Map Reduce work (taking
`Period` seconds) over `Size` new processes.  

The circular graph will show a discrete increase in messages created for
heart_.beam.
The process graph will display a beating tree of dots that would be our beating
heart of ephemeral lightweight processes.
