-module(heart_).

%% heart_: see repeated mapreduces in realtime.
%%   (the name ‘heart’ is reserved…)

-export([beat/3]).

%% API
%%   heart_:beat(Interval, Steps, Workers).
%% Creates an amount of Workers processes every Interval seconds,
%%   a number of Steps time.
%% A 1second-periodical little heart beating would look like this:
%%   heart_:beat(1, 50, 100).

-spec beat(pos_integer(), pos_integer(), pos_integer()) -> ok.

beat(_, 0, _) -> ok;
beat(Interval, Steps, Workers) ->
    Self = self(),
    [receive M -> M end
     || _ <-   [spawn(fun() -> do(master, Self, Interval) end)]
            ++ [spawn(fun() -> do(worker, Self, Interval) end)
                || _ <- lists:seq(1, Workers - 1)]],
    beat(Interval, Steps - 1, Workers).



%% Internals

do(master, Parent, Interval) ->
    timer:sleep(1000 * Interval),
    Parent ! really_done;
do(worker, Parent, Interval) ->
    timer:sleep(1000 * random:uniform(Interval)),
    Parent ! done.

%% End of module
