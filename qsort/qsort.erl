-module(qsort).

%% qsort: A set of quick-sort strategies for realtime viewing.


%% How To Use
%% On a m1_xlarge EC2 instance (4 cores, ∞ RAM), those are suited calls:
%%   qsort:pclever_test(500, 10000).
%%   qsort:naive_test(10000).
%%   qsort:parallel_test(1000).
%% You will be able to see heavy load on CPU, a large amount of messages passed
%%   and a fair use of all the machines' cores.
%% It essentially demonstrates how a simple and well-known algorithm
%%   fits a multi-core architecture; and how its main parameters can be
%%   used to best fit said machine.


%% API
-export([naive/1,
         parallel/1,
         pclever/2]).

%% qsort:naive/1
%%   Applies the basic (recursive, non-parallel, trivial) Quick Sort.
%% qsort:parallel/1
%%   Spawns a new process at each recursion. Naive parallelism.
%% qsort:pclever/2
%%   Spawns a finite amount of processes to prevent overhead, then falls back
%%   to the naive sequential version.


%% Unit testing (using reversed lists)
-export([naive_test/1,
         parallel_test/1,
         pclever_test/2]).


%% Notes
%%   A "+" is displayed when entering each function,
%%   a "-" when leaving.


-spec naive([number()]) -> [number()].

naive([]) -> [];
naive([Pivot|Rest]) ->
    io:format("+"),
    Sorted =
        naive([E || E <- Rest, E < Pivot])
        ++ [Pivot] ++
        naive([E || E <- Rest, E >= Pivot]),
    io:format("-"),
    Sorted.

naive_test(Size) ->
    %% [1,2,3,4,5] = naive([3,4,5,2,1]),
    naive([random:uniform(10) || _ <- lists:seq(1, Size)]).



-spec parallel([number()]) -> [number()].

parallel([]) -> [];
parallel([Pivot|Rest]) ->
    io:format("+"),
    {Left, Right} = dichotomic_MR(fun parallel/1,
                                  [E || E <- Rest, E < Pivot],
                                  [E || E <- Rest, E >= Pivot]),
    Sorted = Left ++ [Pivot] ++ Right,
    io:format("-"),
    Sorted.

parallel_test(Size) ->
    parallel([random:uniform(10) || _ <- lists:seq(1, Size)]).



-spec pclever(pos_integer(), [number()]) -> [number()].

pclever(_, []) -> [];
pclever(0, List) -> naive(List);
pclever(N, [Pivot|Rest]) ->
    io:format("+"),
    {Left, Right} = dichotomic_MR(fun(X) -> pclever(N - 1, X) end,
                                  [E || E <- Rest, E < Pivot],
                                  [E || E <- Rest, E >= Pivot]),
    Sorted = Left ++ [Pivot] ++ Right,
    io:format("-"),
    Sorted.

pclever_test(N, Size) ->
    pclever(N, [random:uniform(10) || _ <- lists:seq(1, Size)]).




%% Internals

dichotomic_MR(F, L1, L2) ->
    Self = self(),
    L = [receive M -> M end
         || _ <- [spawn_link(fun() -> Self ! {left, catch F(L1)} end),
                  spawn_link(fun() -> Self ! {right,catch F(L2)} end)]],
    {proplists:get_value(left,L), proplists:get_value(right,L)}.


%% End of module
