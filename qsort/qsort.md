# qsort.erl

## qsort:naive/1
Performs the usual recursive Quicksort over a list of numbers,
which means it does no message passing and is very CPU intensive.

## qsort:parallel/1
Spawns two new iterations in new processes,
thus creating a lot of processes and the double of messages.
The issue of this approach becomes clear graphically, as we
see that too much time and memory is spent mapping and reducing processes instead of doing actual sorting.

## qsort:pclever/2
A parameterized attempt at reducing `qsort:parallel/1`'s overhead.
In order to do this, the parameter sets the maximum of processes to spawn.
(Actually, for a parameter `N`, the maximum will be set to `2 * N`).
Then, the function falls back to `qsort:naive/1`.  

By moving the parameter closer to the machine's amount of cores and
analyzing the realtime graphs, one can find the optimal combination.
