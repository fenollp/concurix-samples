# bench

`suite.sh` is a Command-Line-only program that demonstrates Concurix's
performance enhanced Erlang.  

Concurix's team has been able to improve the HiPE compiler, recompiled
the standard libraries and we are now going to see the results.

## How to Bench

To compare Concurix's Erlang against the original Erlang (both R15B02),
execute this line on your _Concurix EC2 instance_:

    ERLC=erlc ERL=erl ./suite.sh bench.erl


Then this line (provided paths to the original Erlang executables are named
`/opt/erlang/erl` and `/opt/erlang/erlc`):

    ERLC=/opt/erlang/erlc ERL=/opt/erlang/erl ./suite.sh bench.erl

## What Happens Inside

This benchmark tries to get the most out of the `math` module.
Among other figures, `bench:main()` prints on the screen the median
execution time of each beanchmark it contains, in µseconds.  

One may then compare those numbers obtained from those two different Erlangs.
