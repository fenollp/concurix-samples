#!/bin/bash

## Sequentially compiles then calls $file:main().

[[ $# -eq 0 ]] && echo "Usage: $0  ‹path to files exporting main/0›" && exit 1

[[ "$ERLC" = '' ]] && echo 'Set ERLC to something like `which erlc`.' && exit 2
[[ "$ERL" = '' ]] && echo 'Set ERL to something like `which erl`.' && exit 2


file="$1"
module=`basename $file`
module=${module%.erl}

echo "$ERLC +native +\"{hipe, [o3]}\" $file"
$ERLC +native +"{hipe, [o3]}" $file

echo "$ERL -smp enable -noshell -run $module main"
$ERL -smp enable -noshell -run $module main


sleep 1 # Delay for the system to switch context.
echo
shift && [[ "$1" != '' ]] && $0 $*

