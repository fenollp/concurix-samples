-module(bench).

%% bench: do some number crunching to compare HiPE compilers.

-export([main/0]).
-export([avg/4]).
-export([avg__naive_fib/2, avg__crunch/3, avg__pythagorean_triplets/2]).
-export([naive_fib/1, crunch/2, pythagorean_triplets/1]).


main() ->
    io:format("Some number crunching to compare HiPE native compilers.\n\n"),
    avg__crunch(8.0e6, 8.0e6+1.0e3, 1.0e3),
    receive after round(1.0e3) -> ok end,
    avg__pythagorean_triplets(1.0e2, 1.0e2),
    receive after round(1.0e3) -> ok end,
    avg__naive_fib(33, 1.0e2),
    erlang:halt(0).



avg__naive_fib(X, N) ->
    A = [round(X)],
    io:format("Benchmarking naive_fib(~p)\n", A),
    avg(?MODULE, naive_fib, A, round(N)).

avg__crunch(Begin, End, N) ->
    A = [round(Begin), round(End)],
    io:format("Benchmarking crunch(~p, ~p)\n", A),
    avg(?MODULE, crunch, A, round(N)).

avg__pythagorean_triplets(X, N) ->
    A = [round(X)],
    io:format("Benchmarking pythagorean_triplets(~p)\n", A),
    avg(?MODULE, pythagorean_triplets, A, round(N)).


%% Internals

%% A slow implementation of finding the nth Fibonacci series element.
naive_fib(0) -> 0;
naive_fib(1) -> 1;
naive_fib(N) when N > 1 ->
    naive_fib(N - 1) + naive_fib(N - 2).

%% Does pointless number crunching.
crunch(Begin, End) ->
    lists:foldl(fun(X, Carry) ->
                        X1 = math:acos(math:cos(X)),
                        X2 = math:asin(math:sin(X1)),
                        X3 = math:atan(math:tan(X2)),
                        X4 = math:acosh(math:cosh(X3)),
                        X5 = math:asinh(math:sinh(X4)),
                        X6 = math:atanh(math:tanh(X5)),
                        X7 = math:log(math:exp(X6)),
                        X8 = math:pow(math:sqrt(math:sqrt(math:sqrt(X7))), 1/8),
                        math:log(math:exp(
                                   math:acos(math:cos(Carry / math:sqrt(37)))
                                   * math:sqrt(X8 * Carry)
                                   / math:atanh(math:tanh(1 - math:exp(-X8)))))
                end, math:pi(), lists:seq(Begin, End)).

%% Pythagorean triplets (from Erlang documentation).
pythagorean_triplets(N) ->
    [{A, B, C} || A <- lists:seq(1, N-2),
                  B <- lists:seq(A+1, N-1),
                  C <- lists:seq(B+1, N),
                  A + B + C =< N,
                  math:pow(A, 2) + math:pow(B, 2) == math:pow(C, 2)].

%% Average Internals
%% http://www.trapexit.org/Measuring_Function_Execution_Time

avg(M, F, A, N) when N > 0 ->
    L = [element(1, timer:tc(M, F, A)) || _ <- lists:seq(1, N)],
    Length = length(L),
    Min = lists:min(L),
    Max = lists:max(L),
    Med = lists:nth(round((Length / 2)), lists:sort(L)),
    Avg = round(lists:sum(L) / Length),
    io:format("\t Range: ~b - ~b microseconds~n"
              "\t Median: ~b microseconds~n"
              "\t Average: ~b microseconds~n",
              [Min, Max, Med, Avg]).

%% End of module.
