# benchmarks

This is a Command-Line-only program that demonstrates Concurix's performance
enhanced Erlang.  

Concurix's team has been able to improve the HiPE compiler, recompiled
the standard libraries and here are the results.

## How to Bench

    ERLC=erlc ERL=erl ./suite.sh marks/*.erl

This will process every benchmark in marks/ with PATH's `erl` and `erlc`.

### marks/* special licensing

Those benchmarks are from the
[Computer Language Benchmarks Game](http://benchmarksgame.alioth.debian.org/
u64q/benchmark.php?test=nbody&lang=hipe)
and are distributed under a _revised BSD License_ you can find
[here](http://benchmarksgame.alioth.debian.org/license.php).
