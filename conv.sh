#!/bin/bash

mov="$1"
file=${mov%.mov}
[[ "$file" = "$mov" ]] && echo "Usage: $0 ‹list of .mov files›" && exit 1


echo "ffmpeg -i $mov -pix_fmt rgb24 $file.gif"
ffmpeg -i $mov -pix_fmt rgb24 $file.gif
echo "convert -layers Optimize $file.gif ${file}_optimized.gif"
convert -layers Optimize $file.gif ${file}_optimized.gif

shift && [[ "$1" != '' ]] && $0 $*
