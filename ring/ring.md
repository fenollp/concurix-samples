# Ring message passing topology

## ring:spin/2
`ring:spin(Size, Rounds)` will create a ring of `Size` actors (processes)
and pass a message from one to the other an amount of `Rounds` times.  

This is a famous example of bad concurrent behavior (a bottleneck),
as `Size * Rounds` messages get shared, one at a time, each process waiting
for a message.  
A large amount of memory is thus used, but more importantly the number of
messages exchanged is way to high and this can be spotted on the circular
graph.
