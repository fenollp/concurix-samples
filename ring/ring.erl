-module(ring).

%% ring: message passing ring for realtime viewing.


%% How To Use
%% To start a ring of M actors and do N rounds, call:
%%   ring:spin(M, N).

%% Appropriate call (makes for good visualizations)
%%   ring:spin(100, 1000).
%% Mainly, a a lot (a hundred) process are created,
%%   and 100 * 1000 messages are sent.
%% Too much messages are sent, as the circular graph can show.
%%   (a great part of sent messages get attributed to ring.beam).

%% API
-export([spin/2]).


%% Notes
%% • ring's starting point is marked with a "*"
%% • the ring is unidirectioal, like a linked list


-spec spin(pos_integer(), pos_integer()) -> ok.

spin(Size, Rounds) ->
    Head = spwn(Size - 1),
    io:format("* is ~p.~n", [Head]),
    Head ! {pass_around, Rounds},
    whirl(Head, head).



%% Internals

%% spwn(0, Starter) -> whirl(Starter, Starter);
%% spwn(N, Starter) ->
%%     Pid = spawn(fun() -> spwn(N - 1, Starter) end),
%%     whirl(Pid, Starter).

spwn(M) ->
    lists:foldl(fun(_,Pid) -> spawn(fun() -> whirl(Pid,actor) end) end,
                self(), lists:seq(1, M)).

whirl(Next, head) ->
    receive
        {pass_around, Last} when Last =:= 1 ->
            io:format("* ~p got ~p, sending to ~p~n", [self(), Last, Next]);
        {pass_around, Rounds} ->
            Next ! {pass_around, Rounds - 1},
            io:format("* ~p got ~p, sending to ~p~n", [self(), Rounds, Next]),
            whirl(Next, head)
    end;
whirl(Next, Ty) ->
    receive
        {pass_around, Rounds} ->
            Next ! {pass_around, Rounds},
            io:format("~p got ~p, sending to ~p~n", [self(), Rounds, Next]),
            whirl(Next, Ty)
    end.


%% End of module
